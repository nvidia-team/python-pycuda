Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PyCUDA
Upstream-Contact: Andreas Klöckner <lists@informa.tiker.net>
Source: https://github.com/inducer/pycuda
 https://pypi.python.org/pypi/pycuda/
Files-Excluded: examples/demo_cdpSimplePrint.py
 bpl-subset
 *.egg-info
Disclaimer: Although PyCUDA itself is free software, it depends on CUDA
 which is proprietary NVIDIA software.  Package nvidia-cuda-toolkit,
 dependency of PyCUDA packages, is in non-free which means that,
 according to Policy 2.2.2, PyCUDA needs to be in contrib.

Files: *
Copyright: 2009-2011 Andreas Klöckner <lists@informa.tiker.net>
License: Expat

Files: pycuda/reduction.py
Copyright: 2009-2011 Andreas Klöckner <lists@informa.tiker.net>
License: Expat
Comment:
 Based on code/ideas by Mark Harris <mharris@nvidia.com>.
 .
 Original License:
 .
 Copyright 1993-2007 NVIDIA Corporation.  All rights reserved.
 .
 NOTICE TO USER:
 .
 This source code is subject to NVIDIA ownership rights under U.S. and
 international Copyright laws.
 .
 NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE
 CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR
 IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH
 REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.
 IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 OR PERFORMANCE OF THIS SOURCE CODE.
 .
 U.S. Government End Users.  This source code is a "commercial item" as
 that term is defined at 48 C.F.R. 2.101 (OCT 1995), consisting  of
 "commercial computer software" and "commercial computer software
 documentation" as such terms are used in 48 C.F.R. 12.212 (SEPT 1995)
 and is provided to the U.S. Government only as a commercial end item.
 Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through
 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the
 source code with only those rights set forth herein.

Files: pycuda/curandom.py
Copyright: 1990, RSA Data Security, Inc.
License: BSD-3-clause
 Copyright (C) 1990 RSA Data Security, Inc. All rights reserved.
 .
 License to copy and use this software is granted provided that
 it is identified as the "RSA Data Security, Inc. MD5 Message
 Digest Algorithm" in all material mentioning or referencing this
 software or this function.
 .
 License is also granted to make and use derivative works
 provided that such works are identified as "derived from the RSA
 Data Security, Inc. MD5 Message Digest Algorithm" in all
 material mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning
 either the merchantability of this software or the suitability
 of this software for any particular purpose.  It is provided "as
 is" without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

Files: pycuda/scan.py
Copyright: 2011 Andreas Kloeckner
           2008-2011 NVIDIA Corporation
License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 Derived from code within the Thrust project, https://github.com/thrust/thrust/
 .
 On Debian systems, the full text of the Apache License version 2
 can be found in the file `/usr/share/common-licenses/Apache-2.0'

Files: pycuda/cuda/pycuda-complex*.hpp
Copyright: 1999 Silicon Graphics Computer Systems, Inc.
           1999 Boris Fomitchev
License: BSD-2-clause
 This material is provided "as is", with absolutely no warranty expressed
 or implied. Any use is at your own risk.
 .
 Permission to use or copy this software for any purpose is hereby granted
 without fee, provided the above notices are retained on all copies.
 Permission to modify the code and to distribute modified code is granted,
 provided the above notices are retained, and a notice that the code was
 modified is included with the above copyright notice.

Files: debian/*
Copyright: © 2011-2020 Tomasz Rybak <serpent@debian.org>
 © 2020-2025 Andreas Beckmann <anbe@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
