pycuda (2025.1~dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Update Standards-Version to 4.7.1; no changes necessary.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 21 Feb 2025 10:34:40 +0100

pycuda (2024.1.2~dfsg-4) unstable; urgency=medium

  * Drop python3-numpy version constraint.  (Closes: #1095259)
  * Rebuild against Numpy 2.  (Closes: #1095213)

 -- Andreas Beckmann <anbe@debian.org>  Thu, 06 Feb 2025 11:18:41 +0100

pycuda (2024.1.2~dfsg-3) unstable; urgency=medium

  * Fix building with sphinx 8.  (Closes: #1090119)

 -- Andreas Beckmann <anbe@debian.org>  Fri, 03 Jan 2025 23:31:55 +0100

pycuda (2024.1.2~dfsg-2) unstable; urgency=medium

  * Cherry-pick upstream patch to fix FTBFS with Python 3.13.
    (Closes: #1087926)

 -- Andreas Beckmann <anbe@debian.org>  Wed, 27 Nov 2024 02:10:46 +0100

pycuda (2024.1.2~dfsg-1) unstable; urgency=medium

  * New upstream release.
    - Switch from python3-appdirs to python3-platformdirs.  (Closes: #1067984)
  * Update Standards-Version to 4.7.0; no changes necessary.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 02 Oct 2024 09:03:43 +0200

pycuda (2024.1~dfsg-1) unstable; urgency=medium

  * New upstream release.  (Closes: #1060788)

 -- Andreas Beckmann <anbe@debian.org>  Mon, 15 Jan 2024 11:57:27 +0100

pycuda (2023.1~dfsg-1) unstable; urgency=medium

  * New upstream release.  (Closes: #1055702)

 -- Andreas Beckmann <anbe@debian.org>  Mon, 13 Nov 2023 10:15:06 +0100

pycuda (2022.2.2~dfsg-2) unstable; urgency=medium

  * Build without python-numpy-doc.

 -- Andreas Beckmann <anbe@debian.org>  Mon, 16 Jan 2023 11:10:07 +0100

pycuda (2022.2.2~dfsg-1) unstable; urgency=medium

  * New upstream release.
  * The CUDA toolkit autopkgtests are now in src:nvidia-cuda-samples.
  * Update Standards-Version to 4.6.2; no changes necessary.

 -- Andreas Beckmann <anbe@debian.org>  Tue, 03 Jan 2023 14:08:09 +0100

pycuda (2022.2~dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Drop ancient d/NEWS.

 -- Andreas Beckmann <anbe@debian.org>  Tue, 22 Nov 2022 22:51:06 +0100

pycuda (2022.1~dfsg-5) unstable; urgency=medium

  * autopkgtest: Fix CUDA Code Samples compilation with CUDA 11.6 and GCC 11.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 05 Nov 2022 22:37:00 +0100

pycuda (2022.1~dfsg-4) unstable; urgency=medium

  * autopkgtest: Fix CUDA Code Samples compilation on !amd64.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 26 Oct 2022 00:02:22 +0200

pycuda (2022.1~dfsg-3) unstable; urgency=medium

  * Add superficial autopkgtest for nvidia-openjdk-8-jre: java -version.
  * Use a tag of the CUDA Code Samples that matches the toolkit version.

 -- Andreas Beckmann <anbe@debian.org>  Mon, 17 Oct 2022 03:26:20 +0200

pycuda (2022.1~dfsg-2) unstable; urgency=medium

  * Switch to B-D: nvidia-cuda-toolkit-gcc for a deterministic g++ dependency.
  * Add superficial autopkgtest for cmake find_package(CUDAToolkit).
  * autopkgtest: Ignore failure to compile the <coroutine> header.
  * Add superficial autopkgtests downloading and compiling the CUDA Code
    Samples.

 -- Andreas Beckmann <anbe@debian.org>  Sun, 16 Oct 2022 14:37:22 +0200

pycuda (2022.1~dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Re-add sphinxconfig.py to the package source to allow offline building and
    customization.
  * Refresh patches.
  * Discard automatic changes to *.egg-info/* to fix building twice in a row.
  * Update Lintian overrides.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 06 Jul 2022 14:28:10 +0200

pycuda (2021.1~dfsg-3) unstable; urgency=medium

  * Add salsa-ci.yml.
  * Add superficial autopkgtest checking the usability of the STL headers in
    host code compiled with nvcc (cf. #1006962).
  * Update Standards-Version to 4.6.1; no changes necessary.
  * Update Lintian overrides.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 03 Jun 2022 06:11:21 +0200

pycuda (2021.1~dfsg-2) unstable; urgency=medium

  * Fix boost python library name generation.  (Closes: #1000308)

 -- Andreas Beckmann <anbe@debian.org>  Mon, 22 Nov 2021 11:37:39 +0100

pycuda (2021.1~dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Add Build-Depends: python3-sphinx-copybutton.
  * Continue using the sphinx alabaster theme, furo is not yet packaged.
  * Drop python3-pycuda-dbg.  (Closes: #994324)
  * Update Standards-Version to 4.6.0; no changes necessary.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 22 Sep 2021 13:31:39 +0200

pycuda (2020.1~dfsg1-1) unstable; urgency=medium

  * Use release tarballs from pypi.
  * Build-Depend on dh-sequence-{python3,numpy3,sphinxdoc}.
  * Cleanup debian/rules.

 -- Andreas Beckmann <anbe@debian.org>  Tue, 02 Feb 2021 15:29:22 +0100

pycuda (2020.1~dfsg-1) unstable; urgency=medium

  * New upstream release.  (Closes: #976780) (LP: #1899750)
  * Refresh patches.
  * Let uscan repack the main tarball with a ~dfsg suffix.
  * Add create-compyte-tarball target to pack compyte as a separate component
    tarball.
  * Fix building twice in a row.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 09 Jan 2021 01:55:29 +0100

pycuda (2019.1.2-1) unstable; urgency=low

  [ Tomasz Rybak ]
  * New upstream release.
    * Refresh patches
    * Add bind1 as new build-dependency
  * Add options to get verbose log from pybuild.

  [ Andreas Beckmann ]
  * Allow building for all architectures with nvidia-cuda-toolkit.
  * The bullseye toolchain defaults to linking with --as-needed.
  * Import upstream tarballs into the packaging repository.
  * Use github repository as upstream.
  * Let the get-orig-source target create a reproducible tarball.
  * Switch to debhelper-compat (= 13).
  * Update Standards-Version to 4.5.1; no changes necessary.
  * Rewrite shebang in python examples to python3.
  * Add myself to Uploaders.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 26 Nov 2020 15:04:39 +0100

pycuda (2018.1.1-4) unstable; urgency=medium

  * Remove Python 2 packages, preparing for #937414.
  * Update Standards-Version to 4.4.1; no changes necessary.
  * Use debhelper-compat versioned dependency instead of d/compat
    to mark debhelper compatibility level.
  * Update d/copyright; use debmake suggestion and change name of MIT
    to Expat licence, removing ambiguity.
  * Add upstream GPG key.
  * Remove Replaces: python-pycuda-headers, not-relevant even in oldoldstable.

 -- Tomasz Rybak <serpent@debian.org>  Wed, 06 Nov 2019 23:36:32 +0100

pycuda (2018.1.1-3) unstable; urgency=medium

  * Rebuild with CUDA 9.2 (Closes: #914804)
  * Update debian/watch to new version and to use substitution variables.

 -- Tomasz Rybak <serpent@debian.org>  Sun, 27 Jan 2019 14:23:34 +0100

pycuda (2018.1.1-2) unstable; urgency=medium

  * Fix FTBFS with Boost 1.67 (Closes: #914804)
    * Patch from Ubuntu by Gianfranco Costamagna
  * Add abort-on-upstream-changes to d/source/local-options.
  * Add more hardening options to d/rules.
  * Add lintian override for lack of autopkgtest.
  * Add DEB_BUILD_OPTIONS terse.
  * Update Standards-Version to 4.3.0; no changes necessary.
  * Set compatibility level to 12; no changes necessary.

 -- Tomasz Rybak <serpent@debian.org>  Mon, 07 Jan 2019 23:27:54 +0100

pycuda (2018.1.1-1) unstable; urgency=medium

  [ Andreas Beckmann ]
  * Put package under maintenance by the Debian NVIDIA Maintainers team, move
    Tomasz to Uploaders.
  * Switch Vcs-* URLs to salsa.debian.org.

  [ Tomasz Rybak ]
  * New upstream release (Closes: #903826).
  * Add Rules-Requires-Root to d/control.
  * Update d/copyright links to use https protocol.
  * Add disclaimer to d/copyright describing why PyCUDA is in contrib.
  * Reorder d/control putting Python 3 packages first.
  * Remove unnecessary X-Python{,3}-Version fields.
  * Update Standards-Version to 4.2.1; no changes necessary.
  * Set compatibility level to 11
    * Point python-pycuda-doc.doc-base to main package's doc directory.

 -- Tomasz Rybak <serpent@debian.org>  Wed, 31 Oct 2018 18:59:56 +0100

pycuda (2017.1.1-2) unstable; urgency=medium

  * Rebuild for CUDA 9.1.
  * Update my email to @debian.org.
  * Add Multi-Arch: no for all binary packages.
  * Reorder debian/copyright, per tip from lintian.

 -- Tomasz Rybak <serpent@debian.org>  Fri, 09 Mar 2018 19:07:39 +0100

pycuda (2017.1.1-1) unstable; urgency=low

  * New upstream release.
  * Update Standards-Version to 4.1.3.
    * Use DEB_BUILD_OPTIONS for nodoc option.
  * Patch Sphinx documentation to allow for reproducible builds.
  * Do not try to fetch external Sphinx objects, use ones from Debian
    packages.
  * Use dpkg's pkg-info.mk instead of dpkg-parsechangelog.
  * Update d/rules get-orig-source to remove all git-related files.
    * Remove non-DFSG-compatible example file.
  * Refresh patches.
  * Change Priority of debugging packages to optional.
  * Change debian/watch file to use HTTPS, as suggested by lintian.
  * Use HTTPS URL of upstream URL.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Wed, 27 Dec 2017 22:35:03 +0100

pycuda (2016.1.2+git20161024-1) unstable; urgency=medium

  * New upstream release (Closes: #842035).
  * Build with CUDA 8.0.
  * Set Multi-Arch: foreign for documentation package.
  * Set compatibility level to 10; no changes necessary.
  * Refresh patches.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Thu, 27 Oct 2016 22:21:24 +0200

pycuda (2016.1.2+git20160809-1) unstable; urgency=low

  * New upstream release.
  * Update Standards-Version to 3.9.8; no changes necessary.
  * Build Sphinx documentation using Python 3.
  * Refresh patches.
  * Update debian/copyright links as upstream changed them.
  * Rebuild with new Boost and Sphinx.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Sun, 11 Sep 2016 14:16:13 +0200

pycuda (2016.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop i386 support as it was dropped by NVIDIA in CUDA 7.0.
  * Update Standards-Version to 3.9.7; no changes necessary.
  * Change Vcs-* links to use https.
  * Refresh patches.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Sun, 27 Mar 2016 17:53:59 +0200

pycuda (2015.1.3-1) unstable; urgency=medium

  * New upstream release.
  * Do not build CUDAND extensions on i386 as CUDA does not provide library;
    thanks for Andreas Beckmann for fixing patch.
  * Move to pybuild as build system, simplifying d/rules.
  * Add versions to some of Python dependencies.
  * Refresh fix-setup.patch.
  * Build for Python 3.5.
  * Build with CUDA 6.5, GCC 5, and Boost 1.58 (Closes: #800389).
  * Build documentation with Sphinx 1.3, increasing build reproducibility.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Fri, 02 Oct 2015 09:58:19 +0200

pycuda (2015.1.2-1) unstable; urgency=medium

  * New upstream release.
  * Make the build reproducible by correctly passing changelog
    date to Sphinx - thanks to Juan Picca for patch.
  * Fix debian/watch to work with PyPI.
  * Fix problems with d/copyright pointed by lintian.
  * Add dh-python to Build-Depends.
  * Patch documentation to use MathJax from package, not from the network.
  * Drop use-appdirs.patch, merged by upstream.
  * Refresh other patches.
  * Do not put undistributed tests into documentation package.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Thu, 09 Jul 2015 21:15:35 +0200

pycuda (2014.1-3) unstable; urgency=medium

  * Add dependency to appdirs and use it to create cache directories
    (Closes: #770689).

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Sat, 29 Nov 2014 11:56:04 +0100

pycuda (2014.1-2) unstable; urgency=medium

  * Install OpenGL and sparse modules files for both Python 2 and 3.
  * Fix problems with debian/copyright pointed by lintian.
  * Update Standards-Version to 3.9.6; no changes necessary.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Sun, 05 Oct 2014 18:35:48 +0200

pycuda (2014.1-1) unstable; urgency=medium

  * New upstream release.
  * Remove dependency on libcuda1, leaving nvidia-cuda-toolkit dependency,
    which depend on libcuda1 (Closes: #749428).
  * Change d/rules get-orig-source to pack *.orig.tar with xz, not with gzip.
  * Change d/control to note that upstream stopped supporting Python 2.5.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Sun, 24 Aug 2014 16:23:29 +0200

pycuda (2013.1.1+git20140527-1) unstable; urgency=medium

  * New upstream version.
  * Fix Vcs-Browser link (Closes: #749489).
  * Rebuild to remove Python 3.3 dependency (Closes: #750886).

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Sun, 08 Jun 2014 11:34:24 +0200

pycuda (2013.1.1+git20140310-1) unstable; urgency=low

  * New upstream version.
  * Change python-pycuda-doc recommendation of Python and Python 3 packages
    to suggestions to avoid installing unwanted packages, like in #739173.
  * Rebuild with Python 3.4 support.
  * Update Standards-Version to 3.9.5; no changes necessary.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Sat, 22 Mar 2014 18:57:13 +0100

pycuda (2013.1.1+git20131128-1) unstable; urgency=low

  * New upstream version.
  * Rebuild against CUDA 5.5 (Closes: #730263).

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Thu, 28 Nov 2013 19:46:55 +0100

pycuda (2013.1.1-1) unstable; urgency=low

  * New upstream release.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Wed, 17 Jul 2013 20:18:58 +0200

pycuda (2013.1~git20130626-1) unstable; urgency=low

  * New upstream release, adding new CURAND support.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Sun, 30 Jun 2013 18:10:45 +0200

pycuda (2013.1~git20130616-1) unstable; urgency=low

  * New upstream release.
  * Add Python 3 package.
  * Add *-dbg packages.
  * Remove *-headers package; upstream now ships headers in python directory.
  * Upstream switched to setuptools, update patches to deal with it.
  * Do not call dh_sphinxdoc for architecture dependent builds, build
    documentation in separate step in debian/rules.
  * Fix CUDA library build-dependencies, remove old libcuda1-dev.
  * Add README.source describing get-orig-source in debian/rules.
  * Disable package optimisation option as it might clash with hardening
    compile options.
  * Include tests (all files in test/) in documentation package.
  * Fix VCS-* fields to contain canonical URIs.
  * Update Standards-Version to 3.9.4; no changes necessary.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Thu, 20 Jun 2013 19:41:29 +0200

pycuda (2012.1-1) unstable; urgency=low

  * New upstream release.
  * Change my email.

 -- Tomasz Rybak <tomasz.rybak@post.pl>  Thu, 21 Jun 2012 23:41:50 +0200

pycuda (2011.2.2+git20120601-1) unstable; urgency=low

  * New upstream release.
  * Rebuild against Boost 1.49. Closes: #672745
  * Create separate python-pycuda-headers package preparing for
    Python 3 package.
  * Move python-pyopengl from Recommends to Suggests because package
    can be used for computations without any graphical environment.
  * Update Standards-Version to 3.9.3.
  * Update debian/copyright to official 1.0 format.
  * Update debian/compat to 9, update dependencies.
  * Call dh_numpy in debian/rules for NumPy ABI and ABI dependencies.

 -- Tomasz Rybak <bogomips@post.pl>  Tue, 05 Jun 2012 18:04:40 +0200

pycuda (2011.2.2-1) unstable; urgency=low

  * New upstream release with support for CUDA 4.1.

 -- Tomasz Rybak <bogomips@post.pl>  Wed, 14 Dec 2011 14:57:51 +0100

pycuda (2011.1.3+git20110814-1) unstable; urgency=low

  * Initial release (Closes: #546919)
  * Add option in debian/source/ to unapply patches after build.
  * Add override python-py of pytest package
  * Change build scripts to build library for all available Python versions
  * Change package to use python-setuptools package instead of downloading it
  * Delete jquery.js and replace it with link to script from libjs-jquery
  * Force build scripts not to use boost libraries included in the source
  * Remove messages about missing Boost libraries; they are superfluous
    as package is using Debian ones

 -- Tomasz Rybak <bogomips@post.pl>  Sat, 20 Aug 2011 23:02:55 +0200
