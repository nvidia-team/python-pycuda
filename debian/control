Source: pycuda
Section: contrib/python
Priority: optional
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders:
 Tomasz Rybak <serpent@debian.org>,
 Andreas Beckmann <anbe@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-numpy3,
 dh-sequence-python3,
 libboost-python-dev,
 libboost-thread-dev,
 mesa-common-dev,
 nvidia-cuda-toolkit-gcc,
 pybind11-dev,
 python3-all-dev,
 python3-numpy,
 python3-pybind11,
 python3-pytools,
 python3-setuptools,
Build-Depends-Indep:
 dh-sequence-sphinxdoc <!nodoc>,
 python-mako-doc <!nodoc>,
 python3-doc <!nodoc>,
 python3-sphinx (>= 1.0.7+dfsg) <!nodoc>,
 python3-sphinx-copybutton <!nodoc>,
Standards-Version: 4.7.1
Rules-Requires-Root: no
Homepage: http://mathema.tician.de/software/pycuda
Vcs-Browser: https://salsa.debian.org/nvidia-team/python-pycuda
Vcs-Git: https://salsa.debian.org/nvidia-team/python-pycuda.git

Package: python3-pycuda
Architecture: any
Multi-Arch: no
Depends:
 nvidia-cuda-toolkit,
 python3-platformdirs (>= 2.2.0),
 python3-numpy,
 python3-pytools,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Recommends:
 python-pycuda-doc <!nodoc>,
 python3-mako,
Suggests:
 python3-matplotlib,
 python3-opengl,
 python3-pytest,
Description: Python 3 module to access Nvidia‘s CUDA parallel computation API
 PyCUDA lets you access Nvidia‘s CUDA parallel computation API from Python.
 Several wrappers of the CUDA API already exist–so what’s so special about
 PyCUDA?
  * Object cleanup tied to lifetime of objects. This idiom, often called
    RAII in C++, makes it much easier to write correct, leak- and crash-free
    code.  PyCUDA knows about dependencies, too, so (for example) it won’t
    detach from a context before all memory allocated in it is also freed.
  * Convenience. Abstractions like pycuda.driver.SourceModule and
    pycuda.gpuarray.GPUArray make CUDA programming even more convenient than
    with Nvidia’s C-based runtime.
  * Completeness. PyCUDA puts the full power of CUDA’s driver API at your
    disposal, if you wish.
  * Automatic Error Checking. All CUDA errors are automatically translated
    into Python exceptions.
  * Speed. PyCUDA’s base layer is written in C++, so all the niceties
    above are virtually free.
  * Helpful Documentation.
 .
 This package contains Python 3 modules.

Package: python-pycuda-doc
Section: contrib/doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends:
 fonts-mathjax,
 libjs-mathjax,
 ${misc:Depends},
 ${sphinxdoc:Depends},
Recommends:
 nvidia-cuda-doc,
 python-mako-doc,
 python3-doc,
Suggests:
 python3-pycuda,
Description: module to access Nvidia‘s CUDA computation API (documentation)
 PyCUDA lets you access Nvidia‘s CUDA parallel computation API from Python.
 Several wrappers of the CUDA API already exist–so what’s so special about
 PyCUDA?
  * Object cleanup tied to lifetime of objects. This idiom, often called
    RAII in C++, makes it much easier to write correct, leak- and crash-free
    code.  PyCUDA knows about dependencies, too, so (for example) it won’t
    detach from a context before all memory allocated in it is also freed.
  * Convenience. Abstractions like pycuda.driver.SourceModule and
    pycuda.gpuarray.GPUArray make CUDA programming even more convenient than
    with Nvidia’s C-based runtime.
  * Completeness. PyCUDA puts the full power of CUDA’s driver API at your
    disposal, if you wish.
  * Automatic Error Checking. All CUDA errors are automatically translated
    into Python exceptions.
  * Speed. PyCUDA’s base layer is written in C++, so all the niceties
    above are virtually free.
  * Helpful Documentation.
 .
 This package contains HTML documentation and example scripts.
