#! /usr/bin/make -f

ifeq (,$(filter terse,$(DEB_BUILD_OPTIONS)))
export DH_VERBOSE = 1
export PYBUILD_VERBOSE=1
else
export DH_QUIET = 1
endif

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND = -Wall -pedantic
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/pkg-info.mk

export PYBUILD_DESTDIR_python3=debian/python3-$(DEB_SOURCE)/

ifneq (,$(filter nostrip,$(DEB_BUILD_OPTIONS)))
DEB_CXXFLAGS_MAINT_STRIP=-O2
DEB_CXXFLAGS_MAINT_APPEND=-O0
endif

BUILD_DATE=$(shell LC_ALL=C date -u "+%B %d, %Y" -d "@$(SOURCE_DATE_EPOCH)")
SPHINXOPTS:=-D today=\"$(BUILD_DATE)\"
SPHINXOPTS+=-D html_last_updated_fmt=\"$(BUILD_DATE)\"

%:
	dh $@ --buildsystem pybuild

override_dh_auto_configure:
	./configure.py \
		--no-use-shipped-boost \
		--boost-python-libname=boost_python \
		--boost-thread-libname=boost_thread \
		--cuda-root=/usr --cuda-enable-gl

execute_after_dh_auto_build-indep:
	$(MAKE) -C doc html SPHINXOPTS="$(SPHINXOPTS)"

execute_after_dh_auto_clean:
	! command -v sphinx-build 2>/dev/null || $(MAKE) -C doc clean
	$(RM) Makefile siteconf.py
	$(RM) -r *.egg-info/

execute_after_dh_installexamples-indep:
	hardlink -v -t debian/python-pycuda-doc/usr/share/doc/python-pycuda-doc

# Do not run test automatically - they require CUDA-capable hardware
override_dh_auto_test:
